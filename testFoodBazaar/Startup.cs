﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(testFoodBazaar.Startup))]
namespace testFoodBazaar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
